<?php
include "config.php";

// $pId = $_GET['iProductId'];
// $productEdit = "SELECT * FROM products WHERE iProductId='" . $pId . "'";
// $sql = mysqli_query($con, $productEdit);
// $data = mysqli_fetch_array($sql);

?>
<!doctype html>
    <html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <title>PRODUCTS</title>
    </head>

    <body>
        <div class="container">
            <h1 class="text-center">MY PRODUCT</h1>

                <form  enctype="multipart/form-data" method="POST" id="registraion">
                    <div class="form-group">
                        <label for="file">Images</label>
                        <input type="file" name="pimage" class="form-control" id="file">
                    </div>

                    <div class="form-group">
                        <label for="usr"> Product Name</label>
                        <input type="text" name="pname" class="form-control" placeholder="Enter Product Name" id="usr" required>
                    </div>


                    <div class="form-group">
                            
                            <select class="custom-select" name="pcid">Select Category 
                            <option value=""> All Category  </option> 
                            <?php
                            while ($row = $q->fetch_object()) {?>
                            <option value="<?php echo $row->iCategoryId ?>"><?php echo $row->vCategoryName ?></option>
                            <?php }?>
                `   </div>
                

                    <div class="form-group">
                        <label for="usr">Price</label>
                        <input type="text" name="pprice" class="form-control mb-2 mr-sm-2" placeholder="Enter Price" id="usr">
                    </div>

                    <div class="form-group">
                        <label for="usr"> Sale Price</label>
                        <input type="text" name="psaleprice" class="form-control mb-2 mr-sm-2" placeholder="Enter Sale Price" id="usr">
                    </div>
                  
                   
                   
                    
                    <div class="form-group">
                        <label for="usr">Quantity</label>
                        <input type="text" name="pquantity" class="form-control mb-2 mr-sm-2" placeholder="Choose..." id="usr">
                    </div>

                    <div class="form-group">
                        <label for="usr">Order</label>
                        <input type="text" name="porder" class="form-control mb-2 mr-sm-2" placeholder="Select Order" id="usr">
                    </div>

                    <div class="radio">
                    <label for="file">Status</label>
                        <label><input type="radio" value="1" name="pstatus" checked>Active</label>
                        <label><input type="radio" value="0" name="pstatus">InActive</label>
                    </div>

                   
                        <div class="col text-center">
                            <button type="submit" name="submit" class="btn btn-dark">Add product</button>
                        </div>
                    
                </form>                        
            </div>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body>

    </html>
