<?php
    include "config.php";
    // ===================CATEGORY DISPLAY========================
  
    $categorySelect="SELECT * FROM categories";
    $sql= mysqli_query($con,$categorySelect);
    $total=mysqli_num_rows($sql);
    $path="image/category/";
?>
<!doctype html>
    <html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"></script>
        <title>CATEGORYLIST</title>
        </head>
        <!-- =========================HEADER==================================== -->

    <body>
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="home.php">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login.html">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="register.html">Register</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="addcate.php">Category</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="logout.php">Logout</a>
                </li>

            </ul>
        </nav>

        <form>
            <div class="form-group">
                <div class="container">
                    <h1 class="text-center">CATEGORY LIST</h1>
                    <div class="row">
                        <!-- add -->
                        <div class="col-lg-12">
                            <button class="btn btn-default float-right"> <a class="nav-link" href="addcate.php">Add Category</a></button>
                            <!-- search -->
                        </div>
                        <div class="md-form mt-0 float-right">
                            <input class="form-control text-right" type="text" placeholder="Search" name="search" aria-label="Search" id="search">
                            <a class="nav-link" href="categorysearch.php"></a>
                        </div>
                        
                        <!-- dropdwon -->
                        <div class="dropdown">
                            <button class="btn btn-dark dropdown-toggle" type="button" name="dropdown" data-toggle="dropdown" id="dropdown">Status
                            <!-- <div id="show"> -->
                            <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" id="active">Active</a></li>
                                <li><a href="#" id="inactive">InActive</a></li>
                            </ul>
                            <a class="nav-link" href="categorysearch.php"></a>
                        </div>
                    </div>
                    
                    </div>
                
                 </div>
            </div>

<table class="table">
            <thead>
                <tr>
                    <th>Images</th>
                    <th><i class='fas fa-arrow-up'></i></i>Name<i class='fas fa-arrow-down'></i></th>
                    <th>Order</th>
                    <th>Status</th>
                    <th>Added Date</th>
                    <th>Modified Date</th>
                    <th colspan="2">Action</th>
                 </tr>
            </thead>
        <tbody>
    
        <?php
            while($row=mysqli_fetch_assoc($sql)) {
            echo "<tr>
                    <td><img src=".$path.$row['vCategoryImage']. " width:100px; height:100px; ></td>
                    <td>".$row['vCategoryName']." </td>
                    <td>".$row['iCategoryOrder']." </td>
                    <td>".$row['tiCategoryStatus']."</td>
                    <td>".$row['tsCategoryAddedDate']."</td>
                    <td>".$row['tsCategoryModifiedDate']." </td>
                    <td><a href='Edit.php?iCategoryId=$row[iCategoryId]'>Edit</a></td>
                    <td><a href='addcate.php?iCategoryId=$row[iCategoryId]'>Delete</td>
                 </tr>";
             }
        
        ?>
        </tbody>
        </form>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function(){
             $('#search').keyup(function(){
            var output=$(this).val();
    
                $.ajax({
                    url:"categorysearch.php",
                    method:"post",
                    data:{output:output},
                    success:function(data)
                    {
                        $('tbody').html(data);
                        
                    }
                });
        });
    });
$(document).on('click',function(){
    $("#dropdown").change(function(){ 
      var output1=$(this).val(); 
              
      $.ajax({ 
        method:"POST", 
        url:"categorysearch.php",
        data:{output:output},
        success:function(data){ 
          $('tbody').html(data);
        }
      });
      
    });

  });


</script>

</body>

</html>