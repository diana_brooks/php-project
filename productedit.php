<?php
include "config.php";
$fetch ="select * from categories";
$res=mysqli_query($con,$fetch);
$pId = $_GET['iProductId'];
$productEdit = "SELECT * FROM products WHERE iProductId='" . $pId . "'";
$sql = mysqli_query($con, $productEdit);
$data = mysqli_fetch_array($sql);

?>
<!doctype html>
    <html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <title>PRODUCTS</title>
    </head>

    <body>
    <div class="container">
    <h2 class="text-center">MY CATEGORY</h2>
  
    <form  action="productadd.php" enctype="multipart/form-data" method="POST">
    <input type="hidden"  name="pid" value="<?php echo $data['iProductId']?>" class="form-control" id="usr">
        <div class="form-group">
            <label for="file">Image</label>
           <p><img src="image/product/<?php echo $data['vProductImage']?>"></p>
            <input type="file" name="pimage" class="form-control" id="file">    
        </div>

        <div class="form-group">
            <label for="usr"> Product Name</label>
            <input type="text" name="pname" value="<?php echo $data['vProductName'];?>"class="form-control" id="usr" required>
        </div>
        <div class="form-group">
            <label for="usr"> Product code</label>
            <input type="text"  name="pcode" value="<?php echo $data['iProductCode']?>" class="form-control" id="usr" required>
        </div>
        <div class="form-group">

            <select class="custom-select" name="pcid">
                <option value=""> All Category</option>
                <?php
                     while ($row =mysqli_fetch_array($res)) {?>
                <option value="<?php echo $row['iCategoryId'] ?>"><?php echo $row['vCategoryName'] ?></option>
                <?php }?></select></div>

        <div class="form-group">
            <label for="usr"> Product Price</label>
            <input type="text"  name="pprice" value="<?php echo $data['fProductPrice']?>" class="form-control" id="usr" required>
        </div>
        <div class="form-group">
            <label for="usr">Sale Price</label>
            <input type="text"  name="psaleprice" value="<?php echo $data['fProductSalePrice']?>" class="form-control" id="usr" required>
        </div>
        <div class="form-group">
            <label for="usr"> Product Quantity</label>
            <input type="text"  name="pquantity" value="<?php echo $data['iProductQuantity']?>" class="form-control" id="usr" required>
        </div>

        <div class="form-group">
            <label for="usr">Order</label>
            <input type="text"  name="porder" value="<?php echo $data['iProductOrder']?>" class="form-control" id="usr" required>
        </div>

        <label for="file">Status</label>
        <div class="radio">
            <label><input type="radio" value="1" <?php if($data['tiProductStatus']=='1'){echo 'checked';}?> name="cstatus" checked>Active</label>
            <label><input type="radio" value="0" <?php if($data['tiProductStatus']=='0'){echo 'checked';}?> name="cstatus">InActive</label>
        </div>

        <div class="col text-center">
            <button type="submit" name="Edit" value="Edit" class="btn btn-dark">Add product</button>
        </div>
</div>
</form>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body>

    </html>
