<?php
    include "config.php";
    $productSelect="SELECT * FROM products";
    $sql= mysqli_query($con,$productSelect);
    $total=mysqli_num_rows($sql);

    if($total !=0)
    {
    ?>

<div class="container">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Images</th>
                            <th><i class='fas fa-arrow-up'></i></i>Name<i class='fas fa-arrow-down'></i></th>
                            <th>Order</th>
                            <th>Product code</th>
                            <th>Product Price</th>
                            <th>Product Sale Price</th>
                            <th>product Quantity</th>
                            <th>Status</th>
                            <th>Added Date</th>
                            <th>Modified Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
<?php
  while($row=mysqli_fetch_assoc($sql)) 
     echo "<tr>
                <th>".$row['vProductName']."</th>
                <th>".$row['iProductCode']."</th>
                <th>".$row['fProductPrice']."</th>
                <th>".$row['fProductSalePrice']."</th>
                <th>".$row['iProductQuantity']." </th>
                <th>".$row['iProductOrder']."</th>
                <th>".$row['tiProductStatus']."</th>
                <th>".$row['tsAddedDate']." </th>
                <th>".$row['tsModifiedDate']." </th>
                <th><a href='productedit.php?iProductId=$row[iProductId]'>Edit</a></th>
                <th><a href='productadd.php?iProductId=$row[iProductId]'>Delete</th>
        </tr>";
    }
?>