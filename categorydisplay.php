<?php
    include "config.php";
    error_reporting(0);
    $result="SELECT * FROM categories";
    $sql= mysqli_query($con,$result);
    $total=mysqli_num_rows($sql);

    if($total !=0)
    {
    ?>
    <table class="table">
        <thead>
            <tr>
                <th>Images</th>
                <th><i class='fas fa-arrow-up'></i></i>Name<i class='fas fa-arrow-down'></i></th>
                <th>Order</th>
                <th>No of products</th>
                <th>Status</th>
                <th>Added Date</th>
                <th>Modified Date</th>
                <th colspan="2">Action</th>
             </tr>
        </thead>    
    <tbody>

    <?php
        while( $row=mysqli_fetch_assoc($sql)) 
        echo "<tr>
                <th>".$row['iCategoryId']."</th>
                <th>".$row['vCategoryImage']."</th>
                <th>".$row['vCategoryName']."</th>
                <th>".$row['iCategoryOrder']." </th>
                <th>".$row['tiCategoryStatus']."</th>
                <th>".$row['tsCategoryAddedDate']."</th>
                <th>".$row['tsCategoryModifiedDate']." </th>
                <th><a href='Edit.php?iCategoryId=$row[iCategoryId]'>Edit</a></th>
                <th><a href='addcate.php?iCategoryId=$row[iCategoryId]'>Delete</th>
             </tr>";
    }
?>