<?php 
  include "config.php";
   // ===================PRODUCT DISPLAY========================

   $productSelect="SELECT * FROM products";
   $sql= mysqli_query($con,$productSelect);
   $total=mysqli_num_rows($sql);
   $path="image/product/";
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <title>PRODUCTLIST</title>
</head>

<body>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="home.php">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="login.html">Login</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="register.html">Register</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="addcate.php">Category</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="productadd.php">Product</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="logout.php">Logout</a>
            </li>

        </ul>
    </nav>

    <form>
        <div class="form-group">
            <div class="container">
                <h1 class="text-center">PRODUCT LIST</h1>
                <div class="row">
                    <!-- add -->
                    <div class="col-lg-12">
                        <button class="btn btn-default float-right"><a class="nav-link" href="productadd.php">Add Category</a></button>
                        <!-- search -->
                    </div>
                    <div class="md-form mt-0 float-right">
                        <input class="form-control text-right" type="text"  value="search" name="search" placeholder="Search" aria-label="Search" id="search">
                        <a class="nav-link" href="productsearch.php"></a>
                    </div>
                    <!-- dropdwon -->
                    <div class="dropdown">
                        <button class="btn btn-dark dropdown-toggle" type="button" data-toggle="dropdown">Status
                        <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Active</a></li>
                            <li><a href="#">InActive</a></li>
                        </ul>
                    </div>
                </div>
                <br>
            </div>
            <div class="container">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Images</th>
                            <th><i class='fas fa-arrow-up'></i></i>Name<i class='fas fa-arrow-down'></i></th>
                            <th>Order</th>
                            <th>Product code</th>
                            <th>Product Price</th>
                            <th>Product Sale Price</th>
                            <th>product Quantity</th>
                            <th>Status</th>
                            <th>Added Date</th>
                            <th>Modified Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
            </div>
        </div>
        </div>
    </form>
</body>
<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

<!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
</body>

</html>