    <?php
    include "config.php";
    // ===========================CATEGORY ADD===============================
    if (isset($_POST['submit'])){
        
        $cName=$_POST['cname'];
        $cOrder=$_POST['corder'];
        $cStatus=$_POST['cstatus'];

        $file_name=$_FILES['cimage']['name'];
        $file_tmp=$_FILES['cimage']['tmp_name'];
        $file_size =$_FILES['cimage']['size'];
        $file_type=$_FILES['cimage']['type'];
      
        $extensions= array("image/jpeg","image/jpg","image/png");
        if (!in_array($file_type, $extensions)) {
            echo "<script>alert('pls Select Only jpeg/jpg/png');window.location.href='addcate.php';</script>";
        }
        else{
            $new_name = rand();
            $path="image/category/".$file_name;
            $imageshow =time().'-'.$new_name.'-'.$file_name;
            $path="image/category/".$imageshow;
            move_uploaded_file($file_tmp,$path);
           
    $categorySelect="SELECT * FROM categories WHERE vCategoryName='".$cName."'";
    $res=mysqli_query($con,$categorySelect);
    
    if(mysqli_num_rows($res)>0){
        echo  "<script>alert('Category Name is already used!');window.location.href='addcate.php';</script>";;
    }else{
    $categoryInsert="INSERT INTO categories (vCategoryImage,vCategoryName,iCategoryOrder,tiCategoryStatus) VALUES ('".$imageshow."','".$cName."','".$cOrder."','".$cStatus."')";
     echo "Data insert.";   
    $result=mysqli_query($con,$categoryInsert);
    if($result){
            header("Location:categorylisting.php");
        }
    else{
            echo "There was an error while adding Data !!";
        }
        
         }
    }
}
// ===============CATEGORY DELETE==================
    if(isset($_GET['iCategoryId'])){
        $id=$_GET['iCategoryId'];
        
    $categoryDelete="DELETE FROM categories WHERE iCategoryId='$id'";
    $sql= mysqli_query($con,$categoryDelete);
    if($sql)
    {
        echo "Delete successfully";
        header("location:categorylisting.php");
        
    }
    else{
        echo "Error";
    }
}
// =====================CATEGORY EDIT============================
if(isset($_POST['cid'])){
  
    $id=$_POST['cid'];
  
    $cName=$_POST['cname'];
   
    $cOrder=$_POST['corder'];
   
    $cStatus=$_POST['cstatus'];
  
    // $cAddedDate=date("Y-m-d H:i:s.");
    // $cModifiedDate=date("Y-m-d H:i:s");

    $file_name=$_FILES['cimage']['name'];
    $file_tmp=$_FILES['cimage']['tmp_name'];
    $file_size =$_FILES['cimage']['size'];
    $file_type=$_FILES['cimage']['type'];

    $new_name = rand();
    $path="image/category/".$file_name;
    $imageshow =time().'-'.$new_name.'-'.$file_name;
    $path="image/category/".$imageshow;
    move_uploaded_file($file_tmp,$path);
    

     $extensions= array("image/jpeg","image/jpg","image/png");
     if(!in_array($file_type,$extensions)){
            echo "<script>alert('Allowed only jpeg, jpg and png file !');window.location.href='addcate.php';
         </script>";
     }
    if($file_name!=null){
         $categoryEdit="UPDATE categories SET vCategoryImage='".$imageshow."', vCategoryName='".$cName."', iCategoryOrder='".$cOrder."', tiCategoryStatus='".$cStatus."' WHERE iCategoryId='".$id."' " ;
    }else{
        $categoryEdit="UPDATE categories SET vCategoryName='".$cName."', iCategoryOrder='".$cOrder."', tiCategoryStatus='".$cStatus."' WHERE iCategoryId='".$id."' " ;
    }
    if(count($_POST)>0) {
        if(mysqli_query($con, $categoryEdit)){
            echo "Records were updated successfully.";
            header("location:categorylisting.php");
        } else {
            echo "ERROR: Could not able to execute $categoryEdit. " . mysqli_error($con);
        }
    }
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
   
</head>

<body>
    
<!-- =========================HEADER===================================== -->
    <div class="container">
        <h2 class="text-center">MY CATEGORY</h2>
      
        <form enctype="multipart/form-data" method="POST" id="registraion">
            <div class="form-group">
                
                <label for="file">Image</label>
                <input type="file" name="cimage" class="form-control" id="file">  
               
            </div>

            <div class="form-group">
                <label for="usr"> Category Name</label>
                <input type="text" name="cname"class="form-control" placeholder="Enter Category Name" id="usr" required>
            </div>

            <div class="form-group">
                <label for="usr">Order</label>
                <input type="text"  name="corder" class="form-control" placeholder="Select Order" id="usr" >
            </div>
            <label for="file" >Status</label>
            <div class="radio">
                <label><input type="radio" value="1" name="cstatus" checked>Active</label>
                <label><input type="radio" value="0" name="cstatus">InActive</label>
            </div>

            <div class="col text-center">
                <button type="submit" name="submit" class="btn btn-dark">Add Category</button>
            </div>
    </div>
    </form>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js"></script>
    <script src="categoryvalidation.js"></script>
</body>

</html>