<?php
include "config.php";

    $cId=$_GET['iCategoryId'];
    $categorySelect="SELECT * FROM categories WHERE iCategoryId='".$cId."'";
    $sql= mysqli_query($con,$categorySelect);
    $data=mysqli_fetch_array($sql);

?>
<!DOCTYPE html>
<html lang="en">

<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>

<div class="container">
    <h2 class="text-center">MY CATEGORY</h2>
  
    <form  action="addcate.php" enctype="multipart/form-data" method="POST">
    <input type="hidden"  name="cid" value="<?php echo $data['iCategoryId']?>" class="form-control" id="usr">
        <div class="form-group">
            <label for="file">Image</label>
           <p><img src="image/category/<?php echo $data['vCategoryImage']?>"></p>
            <input type="file" name="cimage" class="form-control" id="file">    
        </div>

        <div class="form-group">
            <label for="usr"> Category Name</label>
            <input type="text" name="cname" value="<?php echo $data['vCategoryName'];?>"class="form-control" id="usr" required>
        </div>

        <div class="form-group">
            <label for="usr">Order</label>
            <input type="text"  name="corder" value="<?php echo $data['iCategoryOrder']?>" class="form-control" id="usr" required>
        </div>
        <label for="file">Status</label>
        <div class="radio">
            <label><input type="radio" value="1" <?php if($data['tiCategoryStatus']=='1'){echo 'checked';}?> name="cstatus" checked>Active</label>
            <label><input type="radio" value="0" <?php if($data['tiCategoryStatus']=='0'){echo 'checked';}?> name="cstatus">InActive</label>
        </div>

        <div class="col text-center">
            <button type="submit" name="Edit" value="Edit" class="btn btn-dark">Add Category</button>
        </div>
</div>
</form>
</body>

</html>

