$(document).ready(function() {
    $('#registraion').validate({
        rules: {
            cname: {
                required: true
            },
            corder: {
                required: true
            }
        },
        messages: {
            cname: {
                required: 'Category Name is Mandatory !'

            },
            corder: {
                required: '**Please enter Order !'
            }

        },
    });

    $(document).ready(function() {
        $('#login').validate({
            rules: {
                Email: {
                    required: true
                },
                password: {
                    required: true
                }
            },
            messages: {
                Email: {
                    required: '**please enter email !'
                },
                password: {
                    required: '**please enter password !'

                }

            },
        });
    });
    $(document).ready(function() {
        $('#register').validate({
            rules: {
                Username: {
                    required: true
                },
                Email: {
                    required: true
                },
                password: {
                    required: true
                },
                contact: {
                    required: true
                }
            },
            messages: {
                Username: {
                    required: 'Username is Mandatory !'
                },
                Email: {
                    required: '**please enter email !'
                },
                password: {
                    required: '**please enter password !'

                },
                contact: {
                    required: '**please enter contact !'
                }

            },
        });

    });
});