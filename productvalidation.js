$(document).ready(function() {
    $('#registraion').validate({
        rules: {
            pname: {
                required: true
            },
            pcode: {
                required: true
            },
            pprice: {
                required: true
            },
            psaleprice: {
                required: true
            },
            pquantity: {
                required: true
            },
            pOrder: {
                required: true
            },
            date: {
                required: true
            },
        },
        messages: {
            pname: {
                required: 'product Name is Mandatory !'
            },
            pcode: {
                required: '**Please enter code !'
            },
            pprice: {
                required: '**Please enter price !'
            },
            psaleprice: {
                required: '**Please enter saleprice !'
            },
            pquantity: {
                required: '**Please enter quantity !'
            },
            pOrder: {
                required: '**Please enter Order !'
            },
            date: {
                required: '**Please enter date !'
            }

        },
    });

    $(document).ready(function() {
        $('#login').validate({
            rules: {
                Email: {
                    required: true
                },
                password: {
                    required: true
                }
            },
            messages: {
                Email: {
                    required: '**please enter email !'
                },
                password: {
                    required: '**please enter password !'

                }

            },
        });
    });
    $(document).ready(function() {
        $('#register').validate({
            rules: {
                Username: {
                    required: true
                },
                Email: {
                    required: true
                },
                password: {
                    required: true
                },
                contact: {
                    required: true
                }
            },
            messages: {
                Username: {
                    required: 'Username is Mandatory !'
                },
                Email: {
                    required: '**please enter email !'
                },
                password: {
                    required: '**please enter password !'

                },
                contact: {
                    required: '**please enter contact !'
                }

            },
        });

    });
});